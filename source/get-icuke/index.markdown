---
layout: page
title: "Get iCuke"
date: 2011-10-05 18:07
comments: true
sharing: true
footer: true
---

You can install iCuke from Ruby Gems - `gem install iCuke` will do the trick.

You can file bugs etc over on the (new) [official Github repository here](https://github.com/BlueFrogGaming/icuke)
